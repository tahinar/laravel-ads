<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * Allow mass assignment
     */
    protected $guarded = [];

}
