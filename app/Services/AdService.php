<?php

namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Ad;
use phpDocumentor\Reflection\PseudoTypes\False_;

class AdService
{
    /**
     * Retrieve paginated Ad models. Search parameters maybe provided
     *
     * @param string $orderBy
     * @param string $orderDirection
     * @param int $perPage 
     * @return LengthAwarePaginator
     */
    
    public function all(string $orderBy = "id", string $orderDirection = "ASC", int $perPage = 10): LengthAwarePaginator
    {        
        $Ads = Ad::orderBy($orderBy, $orderDirection)
                    ->when(request()->has('title'), function ($q, $value) {
                        $q->where('title', 'LIKE', '%' . request('title') . '%');
                    })
                    ->when(request()->has('description'), function ($q, $value) {
                        $q->where('description', 'LIKE', request('description'));
                    })
                    ->paginate($perPage);
        
        return  $Ads;                      
    }

    /**
     * Find a Ad by its id
     * 
     * @param int $id
     * @param array fields Fields to show in response
     * @return array|False
     */
    public function findById(int $id, array $fields = array()): array|False
    {
        $ad = Ad::findOrFail($id);
        
        if (is_null($ad)) {
            return false;
        }

        $data = [
            'title' => $ad->title,
            'images' => $ad->images,
            'price' => $ad->price
        ];

        foreach($fields as $field) {
            if (isset($ad->{$field})) {
                $data[$field] = $ad->{$field};
            }
        }
        
        return $data;
    }

    /**
     * Create a Ad
     * 
     * @param Request $request
     * @return Vehicule|False
     */
    public function create(Request $request) 
    {
        return Ad::create($request->all());
    }

    public function update(int $id, Request $request): ?Ad
    {        
        $Ad = Ad::findOrFail($id);
        
        if ($Ad->update($request->all())) {
            return $Ad;
        }
    }

    /**
     * Delete a Ad by its id
     * 
     * @param int $id
     * @return boolean
     */
    public function destroy(int $id): bool
    {
        if (Ad::find($id)) {
            return Ad::destroy($id);
        } else {
            return false;
        }
        
    }
}