<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdCollection;
use App\Http\Requests\AdPostRequest;
use App\Services\AdService;

use Illuminate\Http\Request;
use App\Ad;

class AdController extends Controller
{
    protected $adService;
    
    public function __construct(AdService $adService)
    {
        $this->adService = $adService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderBy = $_GET['orderBy'] ?? "id";
        $orderDirection = $_GET['orderDirection'] ?? "ASC";
        $perPage = $_GET['perPage'] ?? 10;

        $ads = $this->adService->all($orderBy, $orderDirection, $perPage);

        return AdCollection::collection($ads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdPostRequest $request)
    {
        $ad = $this->adService->create($request);
        
        if($ad instanceof Ad) {
            return Response()->json([
                'ID' => $ad->id
            ], 200);
        } else {
            $data = [
                'message' => 'Error'
            ];
            return Response()->json($data, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // pass fields
        $fields = $_REQUEST['fields'] ?? array();
        $fields = (array) $fields;

        $ad = $this->adService->findById($id, $fields);
        
        return Response()->json([
            'data' => $ad
        ], 200);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdPostRequest $request, $id)
    {
        $request->validate();

        $ad = $this->adService->update($id, $request);
        
        return Response()->json([
            'data' => new AdCollection($ad)
        ], 200);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
