<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Ad;
use Tests\TestCase;

class AdTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** We can add a ad */
    public function test_ad_can_be_added()
    {
        $this->withoutExceptionHandling();

        $data = [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->paragraph(2),
            'price' => $this->faker->randomFloat(2, 0, 1000),
            'images' => $this->faker->imageUrl(640, 480, 'animals', true)
        ];

        $this->post('api/ads', $data)
             ->assertStatus(200)
             ->assertJsonStructure(
                [
                    'ID'
                ]
             );

        $ads = Ad::all();

        $this->assertEquals(1, count($ads));
    }

    /** Single ad can be shown */
    public function test_ad_can_be_shown()
    {
        $this->withoutExceptionHandling();

        $data = [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->paragraph(2),
            'price' => $this->faker->randomFloat(2, 0, 1000),
            'images' => $this->faker->imageUrl(640, 480, 'animals', true)
        ];
        $ad = Ad::create($data);

        $this->json('GET', "api/ads/{$ad->id}")
             ->assertStatus(200)
             ->assertJson(
                [ 
                    'data' =>[
                        'title' => $ad->title,
                        'images' => $ad->images,
                        'price' => $ad->price,
                    ]
                ]    
             );
    }
}
