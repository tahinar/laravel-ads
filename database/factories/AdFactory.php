<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ad;
use Faker\Generator as Faker;

$factory->define(Ad::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(2),
        'price' => $faker->randomFloat(2, 0, 1000),
        'images' => $faker->imageUrl(640, 480, 'animals', true)
    ];
});
